const errors = {
  default: () => ({
    error: {
      type: 'internal_server_error',
      message: 'Internal Server Error',
    },
  }),
  400: ({ error, params = null }) => {
    const result = {
      error: {
        type: 'invalid_param_error',
        message: error.message,
      },
    };

    if (params) result.error.params = params;

    return result;
  },
  404: ({ request }) => ({
    error: {
      type: 'invalid_request_error',
      message: `Unable to resolve the request ${request.path}`,
    },
  }),
};

module.exports = errors;
