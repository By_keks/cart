const test = require('ava');

const defaultReq = {
  method: 'get',
  body: null,
  path: '',
};

const defaultRes = {
  body: null,
  status: 200,
};

function testing(agent, baseApiUrl) {
  return {
    requestTest({
      title,
      req = defaultReq,
      res = defaultRes,
    }) {
      return test.cb(title, (t) => {
        const params = {
          req: Object.assign({}, defaultReq, req),
          res: Object.assign({}, defaultRes, res),
        };

        const request = agent[params.req.method](`${baseApiUrl}${params.req.path}`);

        params.req.body && request.send(params.req.body);

        request.set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect('set-cookie', /connect.sid/);

        params.res.body
          ? request.expect(params.res.status, params.res.body, t.end)
          : request.expect(params.res.status, t.end);
      });
    },
  };
}

module.exports = testing;
