const { decamelize } = require('humps');

function validationErrorFormatter({ msg, param }) {
  const [code, message] = msg.split(':');

  return {
    code,
    message,
    name: decamelize(param),
  };
}

module.exports = validationErrorFormatter;
