const session = require('express-session');
const bodyParser = require('body-parser');
const express = require('express');
const morgan = require('morgan');

const logger = require('../utils/logger');

/* middlewares */
const {
  responseFormatter,
  paramsFormatter,
  pageNotFound,
  errorHandler,
} = require('../middlewares');

const router = require('./router');

require('dotenv-flow').config({ default_node_env: 'development' });

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

if (process.env.NODE_ENV !== 'test') {
  app.use(morgan('combined', { stream: logger.stream }));
}

app.use(session({
  secret: process.env.COOKIE_SECRET,
  cookie: { maxAge: parseInt(process.env.COOKIE_MAX_AGE, 10) },
  saveUninitialized: true,
  resave: false,
}));

app.use(paramsFormatter);
app.use(responseFormatter);
app.use('/api', router);

app.use(errorHandler);
app.use(pageNotFound);


module.exports = app;
