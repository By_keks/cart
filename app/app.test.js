const supertest = require('supertest');

const { errors } = require('../constants');
const testing = require('../utils/testing');
const app = require('./app');

const { requestTest } = testing(supertest.agent(app), '');

requestTest({
  title: 'return 404 error',
  req: {
    path: '/api/nonexistentmethod',
  },
  res: {
    status: 404,
    body: errors[404]({ request: { path: '/api/nonexistentmethod' } }),
  },
});
