const express = require('express');

/* compoentns */
const products = require('../components/products');
const carts = require('../components/carts');

/* middlewares */
const {
  validationErrorsHandler,
  injectProduct,
  injectCart,
} = require('../middlewares');

const router = express.Router();

router.use('/cart', carts.api({
  beforeFilters: {
    get: [injectCart],
    post: [injectCart, injectProduct],
    delete: [injectCart, injectProduct],
  },
  validationErrorsHandler,
}));

router.use('/products', products.api());

module.exports = router;
