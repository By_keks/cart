#!/usr/bin/env node

/**
 * Module dependencies.
 */

const http = require('http');
const app = require('../app');
const {
  logger,
  normalizePort,
  httpErrorHandler,
} = require('../utils');

/**
 * Get port from environment and store in Express.
 */

const port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

/**
 * Create HTTP server.
 */

const server = http.createServer(app);

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  const addr = server.address();
  const bind = typeof addr === 'string'
    ? `pipe ${addr}`
    : `port ${addr.port}`;

  logger.info(`Listening on ${bind}`);
}

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', httpErrorHandler(port));
server.on('listening', onListening);
