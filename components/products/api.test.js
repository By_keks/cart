const supertest = require('supertest');

const products = require('../../seeds/products.json');
const testing = require('../../utils/testing');
const app = require('../../app');

const { requestTest } = testing(supertest.agent(app), '/api/products');

requestTest({
  title: 'get products list',
  res: {
    body: {
      data: products,
    },
  },
});
