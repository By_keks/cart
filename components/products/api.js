const express = require('express');
const Controller = require('./controller');

function generateRouter() {
  const router = express.Router();

  router.get('/', Controller.index);

  return router;
}

module.exports = generateRouter;
