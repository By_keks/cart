const Product = require('./model');

class Controller {
  static async index(req, res) {
    res.json(await Product.all());
  }
}

module.exports = Controller;
