const api = require('./api');
const model = require('./model');
const controller = require('./controller');

module.exports = {
  api,
  model,
  controller,
};
