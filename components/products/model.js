const productsList = require('../../seeds/products.json');

class Product {
  static async all() {
    return productsList;
  }

  static find(productId) {
    return productsList.find(product => product.id === productId);
  }
}

module.exports = Product;
