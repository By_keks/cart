const express = require('express');

const Controller = require('./controller');
const validators = require('./validators');

function generateRouter({ validationErrorsHandler, beforeFilters = [], afterFilters = [] }) {
  const router = express.Router();

  router
    .get('/', beforeFilters.get, Controller.index)
    .post('/', validators.add, validationErrorsHandler, beforeFilters.post, Controller.add)
    .delete('/:productId', validators.delete, validationErrorsHandler, beforeFilters.delete, Controller.delete);

  afterFilters.map(filter => router.use(filter));

  return router;
}

module.exports = generateRouter;
