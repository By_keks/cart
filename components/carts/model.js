const initialCart = {
  products: {},
};

class Cart {
  constructor(cart = initialCart) {
    this.products = cart.products;
  }

  static calculateTotalSum(products) {
    return Cart
      .productsToArray(products)
      .reduce((sum, id) => {
        const { price, quantity } = products[id];
        return sum + price * quantity;
      }, 0);
  }

  static calculateProductsCount(products) {
    return Cart
      .productsToArray(products)
      .reduce((count, id) => {
        const { quantity } = products[id];
        return count + quantity;
      }, 0);
  }

  static productsToArray(products) {
    return Object.keys(products);
  }

  static serializeProducts(products) {
    return Cart.productsToArray(products).map(i => ({
      id: products[i].id,
      quantity: products[i].quantity,
      sum: Cart.calculateTotalSum([products[i]]),
    }));
  }

  add(product, quantity) {
    this.products[product.id] = Object.assign({}, product, {
      quantity: this.products[product.id]
        ? this.products[product.id].quantity + quantity
        : quantity,
    });

    return this;
  }

  delete(product) {
    if (!(product.id in this.products)) return this;

    this.products[product.id] = Object.assign({}, product, {
      quantity: this.products[product.id].quantity - 1,
    });

    if (this.products[product.id].quantity <= 0) {
      delete this.products[product.id];
    }

    return this;
  }

  serialize() {
    return {
      totalSum: Cart.calculateTotalSum(this.products),
      productsCount: Cart.calculateProductsCount(this.products),
      products: Cart.serializeProducts(this.products),
    };
  }
}

module.exports = Cart;
