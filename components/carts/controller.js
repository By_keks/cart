class Controller {
  static index(req, res) {
    res.json(res.locals.cart.serialize());
  }

  static async add(req, res) {
    res.locals.cart.add(res.locals.product, req.body.quantity);
    res.json(res.locals.cart.serialize());
  }

  static async delete(req, res) {
    res.locals.cart.delete(res.locals.product);
    res.json(res.locals.cart.serialize());
  }
}

module.exports = Controller;
