const supertest = require('supertest');

const { errors } = require('../../constants');
const testing = require('../../utils/testing');
const app = require('../../app');

const { requestTest } = testing(supertest.agent(app), '/api/cart');

requestTest({
  title: 'add one product #1 to cart',
  req: {
    method: 'post',
    body: {
      product_id: 1,
      quantity: 1,
    },
  },
});

requestTest({
  title: 'add one product #2 to cart',
  req: {
    method: 'post',
    body: {
      product_id: 2,
      quantity: 1,
    },
  },
});

requestTest({
  title: 'add ten products #2 to cart',
  req: {
    method: 'post',
    body: {
      product_id: 2,
      quantity: 10,
    },
  },
});

requestTest({
  title: 'remove one product #2 from cart',
  req: {
    method: 'delete',
    path: '/2',
  },
});

requestTest({
  title: 'get cart',
  res: {
    body: {
      data: {
        total_sum: 1550,
        products_count: 11,
        products: [
          {
            id: 1,
            quantity: 1,
            sum: 50,
          },
          {
            id: 2,
            quantity: 10,
            sum: 1500,
          },
        ],
      },
    },
  },
});

requestTest({
  title: 'remove nonexistent product from cart',
  req: {
    method: 'delete',
    path: '/3',
  },
  res: {
    status: 400,
    body: errors[400]({ error: { message: 'Product not found' } }),
  },
});

requestTest({
  title: 'add nonexistent product to cart',
  req: {
    method: 'post',
    body: {
      product_id: 3,
      quantity: 1,
    },
  },
  res: {
    status: 400,
    body: errors[400]({ error: { message: 'Product not found' } }),
  },
});

requestTest({
  title: 'add zero products #2 to cart',
  req: {
    method: 'post',
    body: {
      product_id: 2,
      quantity: 0,
    },
  },
  res: {
    status: 400,
    body: errors[400]({
      error: {
        type: 'invalid_param_error',
        message: 'Invalid data parameters',
      },
      params: [{
        code: 'invalid',
        message: 'Quantity cannot be less than 1 and bigger than 10.',
        name: 'quantity',
      }],
    }),
  },
});

requestTest({
  title: 'add eleven products #2 to cart',
  req: {
    method: 'post',
    body: {
      product_id: 2,
      quantity: 11,
    },
  },
  res: {
    status: 400,
    body: errors[400]({
      error: {
        type: 'invalid_param_error',
        message: 'Invalid data parameters',
      },
      params: [{
        code: 'invalid',
        message: 'Quantity cannot be less than 1 and bigger than 10.',
        name: 'quantity',
      }],
    }),
  },
});

requestTest({
  title: 'add products #2 without params to cart',
  req: {
    method: 'post',
  },
  res: {
    status: 400,
    body: errors[400]({
      error: {
        type: 'invalid_param_error',
        message: 'Invalid data parameters',
      },
      params: [{
        code: 'required',
        message: 'ProductId cannot be blank.',
        name: 'product_id',
      }, {
        code: 'invalid',
        message: 'ProductId must be a number',
        name: 'product_id',
      }, {
        code: 'required',
        message: 'Quantity cannot be blank.',
        name: 'quantity',
      }, {
        code: 'invalid',
        message: 'Quantity must be a number',
        name: 'quantity',
      }, {
        code: 'invalid',
        message: 'Quantity cannot be less than 1 and bigger than 10.',
        name: 'quantity',
      }],
    }),
  },
});
