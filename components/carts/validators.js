const { body, param } = require('express-validator/check');
const { sanitizeBody, sanitizeParam } = require('express-validator/filter');

const validators = {
  add: [
    body('productId')
      .not().isEmpty()
      .withMessage('required:ProductId cannot be blank.')
      .trim()
      .escape()
      .isNumeric()
      .withMessage('invalid:ProductId must be a number'),

    sanitizeBody('productId').toInt(),

    body('quantity')
      .not().isEmpty().withMessage('required:Quantity cannot be blank.')
      .trim()
      .escape()
      .isNumeric()
      .withMessage('invalid:Quantity must be a number')
      .custom(value => (value >= 1 && value <= 10))
      .withMessage('invalid:Quantity cannot be less than 1 and bigger than 10.'),

    sanitizeBody('quantity').toInt(),
  ],
  delete: [
    param('productId')
      .not().isEmpty()
      .withMessage('required:ProductId cannot be blank.')
      .trim()
      .escape()
      .isNumeric()
      .withMessage('invalid:ProductId must be a number'),

    sanitizeParam('productId').toInt(),
  ],
};

module.exports = validators;
