const { validationResult } = require('express-validator/check');
const { validationErrorFormatter } = require('../utils');
const { errors } = require('../constants');

function validationErrorsHandler(req, res, next) {
  const result = validationResult(req).formatWith(validationErrorFormatter);

  if (!result.isEmpty()) {
    return res.status(400).json(errors[400]({
      error: { message: 'Invalid data parameters' },
      params: result.array({ onlyFirstError: false }),
    }));
  }

  return next();
}

module.exports = validationErrorsHandler;
