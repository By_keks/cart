const { model: Cart } = require('../components/carts');

async function injectCart(req, res, next) {
  res.locals.cart = new Cart(req.session.cart);
  return next();
}

module.exports = injectCart;
