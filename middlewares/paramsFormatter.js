const { camelizeKeys, decamelizeKeys } = require('humps');

function paramsFormatter(req, res, next) {
  if (req.body && typeof req.body === 'object') {
    req.body = camelizeKeys(req.body);
  }

  if (req.query && typeof req.query === 'object') {
    req.query = camelizeKeys(req.query);
  }

  const sendJson = res.json;

  res.json = data => sendJson.call(res, decamelizeKeys(data));

  return next();
}

module.exports = paramsFormatter;
