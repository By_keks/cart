const { logger } = require('../utils');
const { errors } = require('../constants');

function errorHandler(err, req, res, next) {
  if (res.headersSent) {
    return next(err);
  }

  const status = err.status || 500;

  if (process.env.NODE_ENV !== 'test') {
    logger.error(`${status} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
  }

  res.status(status);

  return res.json(
    (status in errors)
      ? errors[status]({ request: req, error: err })
      : errors.default(),
  );
}

module.exports = errorHandler;
