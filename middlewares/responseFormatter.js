function responseFormatter(req, res, next) {
  const sendJson = res.json;

  res.json = (data) => {
    let modifiedData = data;
    if (!('error' in data)) modifiedData = { data };
    return sendJson.call(res, modifiedData);
  };

  return next();
}

module.exports = responseFormatter;
