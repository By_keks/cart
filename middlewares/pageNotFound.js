const { errors } = require('../constants');

function pageNotFound(req, res) {
  return res.status(404).json(errors[404]({ request: req }));
}

module.exports = pageNotFound;
