const { model: Product } = require('../components/products');

async function injectProduct(req, res, next) {
  if ('productId' in req.body
    || 'productId' in req.params
  ) {
    res.locals.product = await Product.find(
      req.body.productId || req.params.productId,
    );
  }

  if (!res.locals.product) {
    return next({ status: 400, message: 'Product not found' });
  }

  return next();
}

module.exports = injectProduct;
